using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Subscriptions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Queries
{
    public class CustomerQuery
    {
        public async Task<IEnumerable<Customer>> AllCustomersAsync([Service] IRepository<Customer> customerRepository) =>
            await customerRepository.GetAllAsync();

        public async Task<Customer> GetCustomerById([Service] IRepository<Customer> customerRepository, Guid id)
        {
            Customer gottenCustomer = await customerRepository.GetByIdAsync(id);
            
            return gottenCustomer;
        }

        public async Task<IEnumerable<Customer>> GetRangeCustomersByIds([Service] IRepository<Customer> customerRepository, List<Guid> ids)
        {
            var gottenCustomers = await customerRepository.GetRangeByIdsAsync(ids);
          
            return gottenCustomers;
        }
    }
}