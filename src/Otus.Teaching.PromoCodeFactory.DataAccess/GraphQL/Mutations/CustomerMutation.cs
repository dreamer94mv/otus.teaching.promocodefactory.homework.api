using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotChocolate;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Mutations
{
    public class CustomerMutation
    {
        public async Task<Customer> CreateCustomerAsync([Service] IRepository<Customer> customerRepository,
            string email, string firstName, string lastName, List<CustomerPreference> preferences)
        {
            var newCustomer = new Customer
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                Preferences = preferences
            };
            await customerRepository.AddAsync(newCustomer);

            return newCustomer;
        }

        public async Task<Customer> DeleteCustomerAsync([Service] IRepository<Customer> customerRepository,
            Guid id)
        {
            var deletableCustomer = await customerRepository.GetByIdAsync(id);

            await customerRepository.DeleteAsync(deletableCustomer);

            return deletableCustomer;
        }

        public async Task<Customer> UpdateCustomerAsync([Service] IRepository<Customer> customerRepository,
            Guid id, string email, string firstName, string lastName, List<CustomerPreference> preferences)
        {
            var updateCustomer = await customerRepository.GetByIdAsync(id);

            updateCustomer.Email = email;
            updateCustomer.FirstName = firstName;
            updateCustomer.LastName = lastName;
            updateCustomer.Preferences = preferences;

            await customerRepository.UpdateAsync(updateCustomer);

            return updateCustomer;
        }
    }
}